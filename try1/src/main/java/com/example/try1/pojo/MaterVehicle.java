package com.example.try1.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;

@Entity
public class MaterVehicle {
    @Id
    private String VeRegNo;
    @Column(length = 30)
    private String VeType="Two Wheeler";
    @Column(length = 50)
    private String chassisNo;
    @Column(length = 50)
    private String EngineNo;
    @Column(length = 70)
    private String Owner; 
    @Temporal(TemporalType.DATE)
    private Date RegDate;
    @Column(length = 40)
    private String veBrand;
    @Column(length = 40)
    private String Vevariant; 
    @Column(length = 20)
    private String veColor; 
    @Temporal(TemporalType.DATE)
    private Date prePolicyStartDate; 
    @Temporal(TemporalType.DATE)
    private Date prePolicyEndDate; 
    @Column(length = 60)
    private String prepolicyNo; 
    @Column(length = 40)
    private String prepolicyCompanyName;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "materVehicleRegId")
    List<User> li=new ArrayList<User>();
    
    public List<User> getLi() {
        return li;
    }
    public void setLi(List<User> li) {
        this.li = li;
    }
    public String getVeRegNo() {
        return VeRegNo;
    }
    public void setVeRegNo(String veRegNo) {
        VeRegNo = veRegNo;
    }
    public String getVeType() {
        return VeType;
    }
    public void setVeType(String veType) {
        VeType = veType;
    }
    public String getChassisNo() {
        return chassisNo;
    }
    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }
    public String getEngineNo() {
        return EngineNo;
    }
    public void setEngineNo(String engineNo) {
        EngineNo = engineNo;
    }
    public String getOwner() {
        return Owner;
    }
    public void setOwner(String owner) {
        Owner = owner;
    }
    public Date getRegDate() {
        return RegDate;
    }
    public void setRegDate(Date regDate) {
        RegDate = regDate;
    }
    public String getVeBrand() {
        return veBrand;
    }
    public void setVeBrand(String veBrand) {
        this.veBrand = veBrand;
    }
    public String getVevariant() {
        return Vevariant;
    }
    public void setVevariant(String vevariant) {
        Vevariant = vevariant;
    }
    public String getVeColor() {
        return veColor;
    }
    public void setVeColor(String veColor) {
        this.veColor = veColor;
    }
    public Date getPrePolicyStartDate() {
        return prePolicyStartDate;
    }
    public void setPrePolicyStartDate(Date prePolicyStartDate) {
        this.prePolicyStartDate = prePolicyStartDate;
    }
    public Date getPrePolicyEndDate() {
        return prePolicyEndDate;
    }
    public void setPrePolicyEndDate(Date prePolicyEndDate) {
        this.prePolicyEndDate = prePolicyEndDate;
    }
    public String getPrepolicyNo() {
        return prepolicyNo;
    }
    public void setPrepolicyNo(String prepolicyNo) {
        this.prepolicyNo = prepolicyNo;
    }
    public String getPrepolicyCompanyName() {
        return prepolicyCompanyName;
    }
    public void setPrepolicyCompanyName(String prepolicyCompanyName) {
        this.prepolicyCompanyName = prepolicyCompanyName;
    }
    @Override
    public String toString() {
        return "MaterVehicle [EngineNo=" + EngineNo + ", Owner=" + Owner + ", RegDate=" + RegDate + ", VeRegNo="
                + VeRegNo + ", VeType=" + VeType + ", Vevariant=" + Vevariant + ", chassisNo=" + chassisNo
                + ", prePolicyEndDate=" + prePolicyEndDate + ", prePolicyStartDate=" + prePolicyStartDate
                + ", prepolicyCompanyName=" + prepolicyCompanyName + ", prepolicyNo=" + prepolicyNo + ", veBrand="
                + veBrand + ", veColor=" + veColor + "]";
    } 

    
}
